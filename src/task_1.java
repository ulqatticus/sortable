/**
 * Created by Slavik on 05.09.17.
 */
public class task_1 {

    // sort methode
    public static int[] sort(int mass []) {

        for( int i = 0; i<mass.length; i++){
            for( int j = i+1; j<mass.length; j++){

                if(mass[i]<mass[j]){

                    int k = mass[i];
                    mass[i] = mass[j];
                    mass[j] = k;
                }
            }
        }

        return mass;
    }

    public static void main(String[] args) {
        int  massive[] = new int[10];
        // generate random numbers for test
        for(int i = 0; i<massive.length;i++){
            massive[i] = (int) (Math.random() * 10 - 1);
        }
        for(int i =0; i<massive.length; i++){
            System.out.print(massive[i]+" ");
        }

        System.out.println();
        // sort massive here
        task_1.sort(massive);
        for(int i =0; i<massive.length; i++){
            System.out.print(massive[i]+" ");
        }

    }

}
